import React from "react";
import { Provider } from "react-redux";
import { PersistGate } from "redux-persist/integration/react";
import { createMuiTheme, ThemeProvider, ThemeOptions } from "@material-ui/core";

import createStore from "./src/state/createStore";

const storage = createStore();
const store = storage.store;
const persistor = storage.persistor;

const createTheme = () => {
  let templateTheme: ThemeOptions = {};

  templateTheme.palette = {
    primary: {
      main: "#0E71B3"
    }
  }

  templateTheme.overrides = {
    MuiBottomNavigation: {
      root: {
        backgroundColor: "transparent"
      }
    },
    MuiBottomNavigationAction: {
      root: {
        color: "#E0E0E0",
        '&$selected': {
          color: "#FFFFFF"
        }
      }
    }
  }

  return createMuiTheme(templateTheme);
}

export default ({ element }) => {
  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <ThemeProvider theme={createTheme()}>
          {element}
        </ThemeProvider>
      </PersistGate>
    </Provider>
  )
}

import React, { forwardRef } from "react";
import { useDispatch, useSelector } from "react-redux";
import MaterialTable, { Icons } from "material-table";
import {
  AddBox,
  ArrowUpward,
  Check,
  ChevronLeft,
  ChevronRight,
  Clear,
  Delete,
  Edit,
  FilterList,
  FirstPage,
  LastPage,
  Remove,
  SaveAlt,
  Search,
  ViewColumn,
  CheckCircle,
  ErrorOutline
} from "@material-ui/icons";

import { RootState } from "../state/reducers";
import { delete_task, modify_task } from "../state/actions";
import LocationService from "../services/LocationService";
import { Task } from "../state/reducers/tasks";

const MaterialTableIcons: Icons = {
  Add: forwardRef((props, ref) => <AddBox {...props} ref={ref} />),
  Check: forwardRef((props, ref) => <Check color="primary" {...props} ref={ref} />),
  Edit: forwardRef((props, ref) => <Edit color="primary" {...props} ref={ref} />),
  Clear: forwardRef((props, ref) => <Clear color="secondary" {...props} ref={ref} />),
  Delete: forwardRef((props, ref) => <Delete color="primary" {...props} ref={ref} />),
  DetailPanel: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
  Export: forwardRef((props, ref) => <SaveAlt {...props} ref={ref} />),
  Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
  FirstPage: forwardRef((props, ref) => <FirstPage {...props} ref={ref} />),
  LastPage: forwardRef((props, ref) => <LastPage {...props} ref={ref} />),
  NextPage: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
  PreviousPage: forwardRef((props, ref) => <ChevronLeft {...props} ref={ref} />),
  ResetSearch: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
  Search: forwardRef((props, ref) => <Search {...props} ref={ref} />),
  SortArrow: forwardRef((props, ref) => <ArrowUpward {...props} ref={ref} />),
  ThirdStateCheck: forwardRef((props, ref) => <Remove {...props} ref={ref} />),
  ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />),
};

const TasksTable = () => {
  const tasks = useSelector((state: RootState) => state.tasks);
  const dispatch = useDispatch();

  return <MaterialTable
    style={{
      tableLayout: "fixed",
      whiteSpace: "nowrap",
      boxShadow: "none",
      WebkitBoxShadow: "none",
      fontFamily: "sans-serif"
    }}
    icons={MaterialTableIcons}
    columns={[
      { title: "Estado", field: "isDone",
        lookup: {
          true: "Terminada",
          false: "Pendiente"
        }
      },
      { title: "Nombre", field: "name" },
      { title: "Descripción", field: "description" },
      { title: "Fecha de creación", field: "createdAt", editable: "never" },
      { title: "Fecha de actualización", field: "updatedAt", editable: "never" },
      { title: "", field: "isDone", editable: "never",
        lookup: {
          true: <CheckCircle
            fontSize="small"
            color="primary"/>,
          false: <ErrorOutline
            fontSize="small"
            color="secondary"/>
        }
      },
    ]}
    data={tasks}
    title=""
    localization={{
      body: {
        editRow: {
          deleteText: '¿Desea eliminar esta tarea?',
          cancelTooltip: "",
          saveTooltip: "",
        },
        editTooltip: "Editar tarea",
        deleteTooltip: "Eliminar tarea",
      },
      header: {
        actions: "Acciones",
      }
    }}
    editable={{
      onRowUpdate: (newData, oldData) =>
        new Promise(async (resolve, reject) => {
          const position = await LocationService();
          const location = {
            lat: position.coords.latitude,
            lng: position.coords.longitude
          }
          newData.location = location;

          const index = tasks.indexOf(oldData as Task);

          dispatch(modify_task(index, newData));

          resolve();
        }),
      onRowDelete: oldData =>
        new Promise(async (resolve, reject) => {
          const index = tasks.indexOf(oldData as Task);

          dispatch(delete_task(index));

          resolve();
        }),
    }}/>
}

export default TasksTable;
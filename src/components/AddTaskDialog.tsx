import React, { useState } from "react";
import { useDispatch } from "react-redux";
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  TextField
} from "@material-ui/core";

import { add_task } from "../state/actions";
import { Task } from "../state/reducers/tasks";
import LocationService from "../services/LocationService";

interface AddTaskDialogProps {
  isAddingTask: boolean;
  onShowAddTaskDialog: () => void;
}

const AddTaskDialog = ({ isAddingTask, onShowAddTaskDialog }: AddTaskDialogProps) => {
  const [name, setName] = useState('');
  const [description, setDescription] = useState('');

  const dispatch = useDispatch();

  const onAddNewTask = async () => {
    const position = await LocationService();
    const location = {
      lat: position.coords.latitude,
      lng: position.coords.longitude
    }

    const data: Task = {
      isDone: false,
      name,
      description,
      location
    }
    
    dispatch(add_task(data));
    onShowAddTaskDialog();
  }

  return <Dialog open={isAddingTask} onClose={onShowAddTaskDialog} aria-labelledby="form-dialog-title">
  <DialogTitle id="form-dialog-title">Crear tarea nueva</DialogTitle>
  <DialogContent>
    <DialogContentText>
      Escriba nombre y descripción de la nueva tarea.
    </DialogContentText>
      <TextField
        autoFocus
        margin="dense"
        id="name"
        label="Nombre"
        type="text"
        onChange={event => setName(event.target.value)}
        fullWidth/>
      <TextField
        margin="dense"
        id="desc"
        label="Descripción"
        type="text"
        onChange={event => setDescription(event.target.value)}
        fullWidth/>
  </DialogContent>
  <DialogActions>
    <Button onClick={onShowAddTaskDialog} color="secondary">
      Cancelar
    </Button>
    <Button color="primary" onClick={onAddNewTask}>
      Crear
    </Button>
  </DialogActions>
</Dialog>
}

export default AddTaskDialog;
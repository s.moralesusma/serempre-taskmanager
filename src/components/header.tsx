import React from "react"
import { Link } from "gatsby"
import {
  AppBar,
  Toolbar,
  Typography
} from "@material-ui/core";

interface HeaderProps {
  siteTitle?: string
}

const Header = ({ siteTitle }: HeaderProps) => <AppBar position="static">
  <Toolbar>
    <Typography variant="h6" component="h1">
      {siteTitle ? siteTitle : ''}
    </Typography>
  </Toolbar>
</AppBar>

export default Header;

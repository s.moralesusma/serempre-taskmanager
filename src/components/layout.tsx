/**
 * Layout component that queries for data
 * with Gatsby's useStaticQuery component
 *
 * See: https://www.gatsbyjs.com/docs/use-static-query/
 */

import React, { ReactNode } from "react";
import { navigate } from "gatsby"
import { useStaticQuery, graphql, Link } from "gatsby";
import { useDispatch, useSelector } from "react-redux";
import {
  BottomNavigation,
  BottomNavigationAction,
  Box,
  Container,
  makeStyles
} from "@material-ui/core";
import { Assignment, Room } from "@material-ui/icons";

import Header from "./header";
import "./layout.css";
import { RootState } from "../state/reducers";
import { change_route } from "../state/actions";

const useStyles = makeStyles({
  bottomNav: {
    position: "fixed",
    bottom: 0,
    width: "100%",
    backgroundColor: "#0E71B3",
    paddingTop: 8,
  },
});

interface LayoutProps {
  children: ReactNode;
}

const Layout = ({ children }: LayoutProps) => {
  const route = useSelector((state: RootState) => state.route);
  const dispatch = useDispatch();

  const classes = useStyles();

  const data = useStaticQuery(graphql`
    query SiteTitleQuery {
      site {
        siteMetadata {
          title
        }
      }
    }
  `);

  return <Box height="100%">
    <Header siteTitle={data.site.siteMetadata?.title || `Title`} />
    <Container>
      <Box mt={2} mb="64px">
        <main>{children}</main>
      </Box>
    </Container >
    <Box className={classes.bottomNav}>
      <BottomNavigation
        value={route}
        color="primary"
        onChange={(event, newValue) => {
          event.preventDefault();
          dispatch(change_route(newValue));
        }}
        showLabels>
        <BottomNavigationAction onClick={() => navigate('/')} label="Tareas" icon={<Assignment />} />
        <BottomNavigationAction onClick={() => navigate('/map')} label="Mapa" icon={<Room />} />
      </BottomNavigation>
    </Box>
  </Box>
}

export default Layout;

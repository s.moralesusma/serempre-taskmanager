import React, { useState } from "react";
import { Button } from "@material-ui/core";

import Layout from "../components/layout";
import SEO from "../components/seo";
import AddTaskDialog from "../components/AddTaskDialog";
import TasksTable from "../components/TasksTable";

const IndexPage = () => {
  const [isAddingTask, setIsAddingTask] = useState(false);

  const onShowAddTaskDialog = () => setIsAddingTask(!isAddingTask);

  return <Layout>
    <SEO title="Tareas" />
    <Button variant="outlined" color="primary" onClick={onShowAddTaskDialog}>
      Crear Tarea
    </Button>
    <AddTaskDialog
      isAddingTask={isAddingTask}
      onShowAddTaskDialog={onShowAddTaskDialog}/>
    <TasksTable/>
  </Layout>
}

export default IndexPage;

import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import GoogleMapReact from 'google-map-react';
import { Box, makeStyles } from "@material-ui/core";
import { Room } from "@material-ui/icons";

import Layout from "../components/layout";
import SEO from "../components/seo";
import LocationService from "../services/LocationService";
import { Location } from "../state/reducers/tasks";
import { change_route } from "../state/actions";
import { RootState } from "../state/reducers";

const useStyles = makeStyles({
  marker: {
    position: "absolute",
    bottom: 0,
    left: -10
  },
});

const Marker = (props: any) => {
  const classes = useStyles();
  return <Room fontSize="small" color="primary" className={classes.marker}/>
}

const MapPage = () => {
  const [center, setCenter] = useState(undefined as Location | undefined);
  const dispatch = useDispatch();

  useEffect(() => {
    const getLocation = async () => {
      const position = await LocationService();
    
      const newCenter = {
        lat: position.coords.latitude,
        lng: position.coords.longitude
      }

      setCenter(newCenter);
      dispatch(change_route(1));
    }

    getLocation();
  }, []);

  const tasks = useSelector((state: RootState) => state.tasks);

  return <Layout>
    <SEO title="Mapa" />
    <Box height="75vh">
      <GoogleMapReact
        bootstrapURLKeys={{ key: "AIzaSyDlZQBC_tQyhC4on-0oZAmARVwzJn0N2QE" }}
        center={center}
        defaultZoom={10}>
        {tasks.map((task, index) => <Marker key={index} lat={task.location?.lat} lng={task.location?.lng} />)}
      </GoogleMapReact>
    </Box>
  </Layout>
}

export default MapPage;

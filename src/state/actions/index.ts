import moment from "moment";
import { AnyAction } from "redux";
import * as randomToken from "random-token";

import { Task } from "../reducers/tasks";

export const add_task = (data: Task): AnyAction => {
  data.id = randomToken(16);
  data.updatedAt = moment().format("DD-MM-YYY HH:mm");
  data.createdAt = moment().format("DD-MM-YYY HH:mm");

  return {
    type: "ADD_TASK",
    payload: data
  }
}

export const modify_task = (index: number, newData: Task): AnyAction => {
  newData.updatedAt = moment().format("DD-MM-YYY HH:mm");

  const data = {
    index,
    newData
  }

  return {
    type: "MODIFY_TASK",
    payload: data
  }
}

export const delete_task = (index: number): AnyAction => {
  return {
    type: "DELETE_TASK",
    payload: index
  }
}

export const change_route = (index: number): AnyAction => {
  return {
    type: "CHANGE_ROUTE",
    payload: index
  }
}
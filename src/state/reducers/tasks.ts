import { AnyAction } from "redux";

export interface Location {
  lat: number;
  lng: number;
}

export interface Task {
  id?: string;
  isDone: boolean;
  name: string;
  description: string;
  location?: Location;
  createdAt?: string;
  updatedAt?: string;
}

const tasks = (state: Task[] = [], action: AnyAction): Task[] => {
  const newState = [...state];

  switch (action.type) {
    case "ADD_TASK":
      return [...newState, action.payload as Task];
    case "MODIFY_TASK":      
      newState[action.payload.index] = action.payload.newData;

      return [...newState];

    case "DELETE_TASK":
      newState.splice(action.payload, 1);
      return [...newState];

    default:
      return state;
  }
}

export default tasks;
import { AnyAction } from "redux";

const route = (state: number = 0, action: AnyAction): number => {
  switch (action.type) {
    case "CHANGE_ROUTE":
      return action.payload;
    default:
      return state;
  }
}

export default route;
import { combineReducers } from "redux";
import tasks from "./tasks";
import route from "./route";

const reducers = combineReducers({
  tasks,
  route,
});

export default reducers;

export type RootState = ReturnType<typeof reducers>
/**
 * Implement Gatsby's Browser APIs in this file.
 *
 * See: https://www.gatsbyjs.com/docs/browser-apis/
 */
import wrapWithProviders from "./providers";

export const wrapRootElement = wrapWithProviders;
